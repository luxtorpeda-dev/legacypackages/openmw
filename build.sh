#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_manifest_files "$STEAM_APP_ID_LIST"

generate_openmw_cfg () {
	tail -n +2 "$1" | sed -e 's!\(data\|resources\)=/usr/local/\(.*\)!\1=\2!g'
	echo "fallback-archive=Morrowind.bsa"
	echo "fallback-archive=Tribunal.bsa"
	echo "fallback-archive=Bloodmoon.bsa"
	echo "content=Morrowind.esm"
	echo "content=Tribunal.esm"
	echo "content=Bloodmoon.esm"
}

readonly pfx="$PWD/local"
readonly tmp="$PWD/tmp"
mkdir -p "$pfx"
mkdir -p "$tmp"

# build OpenMW
#
pushd "source"
mkdir -p build
cd build
export OSG_DIR="$pfx/lib64"
cmake \
	-DBUILD_LAUNCHER=OFF \
        -DBUILD_OPENCS=OFF \
        -DBUILD_WIZARD=OFF \
	-DBUILD_MYGUI_PLUGIN=OFF \
        -DCMAKE_PREFIX_PATH="$pfx" \
	-DCMAKE_BUILD_TYPE=MinSizeRel \
        ..
make -j "$(nproc)"
DESTDIR="$tmp" make install
popd

mkdir -p "22320/dist/lib/"
cp -rfv "local/"{lib,lib64}/*.so* "22320/dist/lib/"
cp -rfv "local/lib64/"osgPlugins-* "22320/dist/lib/"
cp -rfv "$tmp/usr/local/"{bin,etc,share} "22320/dist/"

# wrapper for launching the game:
#
cp "openmw.sh" "22320/dist/"

# openmw.cfg will be updated based on a Morrowind.ini file
#
generate_openmw_cfg "$tmp/usr/local/etc/openmw/openmw.cfg" > "22320/dist/openmw.cfg"

# default settings need to be in working dir if they are missing from /etc
#
# TODO: what about other files from etc dir? They seem to serve some purpose.
#
cp "$tmp/usr/local/etc/openmw/settings-default.cfg" "22320/dist/"

# wrapper for finding Qt 4.x libs on users' machines
#
# TODO: compile launcher, forcing Qt 5.x if possible
#
cp openmw-launcher-wrapper "22320/dist/"
